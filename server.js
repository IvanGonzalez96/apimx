var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;//puerto 3000

var path = require('path');
var requestjson =require('request-json')
var jwt = require('jsonwebtoken');
var sessionStorage = require('sessionstorage');


//seguro
var bodyparser=require('body-parser')
app.use(bodyparser.json());
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
  next();
})
var movimientosJSON=require('./movimientosv2.json');//carga el fichero
var urlClientes ="https://api.mlab.com/api/1/databases/igonzalez/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"
//var urlClientes ='https://api.mlab.com/api/1/databases/igonzalez/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt&q={"idcliente" : 1234}'
var clienteMLab = requestjson.createClient(urlClientes)
//gestionar cualquier coleccion
var urlMlabRaiz ="https://api.mlab.com/api/1/databases/igonzalez/collections/"
var apikey = "?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"
var clienteMLabRaiz;
var tokensito;//para el getitem del session storage

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);
//tipo de peticion
app.get('/',function(req,res) {//respuesta de la raiz, los parametros son necesarios sin importar el nombre
  res.sendFile(path.join(__dirname,'index.html'))//enviamos el HTML con sendFile

})
//tipo peticion post
app.post('/',function(req,res) {//respuesta de la raiz, los parametros son necesarios sin importar el nombre
  res.send("Hemos recibido su peticion Post")

})
//tipo de peticion Put
app.put('/',function(req,res) {//respuesta de la raiz, los parametros son necesarios sin importar el nombre
  res.send("Hemos recibido su peticion Put cambiada")

})
//tipo de peticion delete
app.delete('/',function(req,res) {//respuesta de la raiz, los parametros son necesarios sin importar el nombre
  res.send("Hemos recibido su peticion Delete")

})
//recurso
//:idcliente definir variable en la URL
app.get('/Clientes/:idcliente',function(req,res){
  res.send("Aqui tiene al cliente numero : "+req.params.idcliente);
})
app.get('/v1/Movimientos',function(req,res){
  res.sendfile("movimientosv1.json")//con fichero f minuscula
})
app.get('/v2/Movimientos',function(req,res){
  res.json(movimientosJSON)//como objeto json primero como variable
})
//usando un parametro de la URL usando JSON
app.get('/v2/Movimientos/:idcliente',function(req,res){
  res.send(movimientosJSON[req.params.idcliente-1] );
})

app.get('/v2/Movimientosquery',function(req,res){
  res.send(req.query);
})

app.post('/v2/Movimientos',function(req,res){
  var nuevo=req.body
  nuevo.id=movimientosJSON.length + 1;
  movimientosJSON.push(nuevo)
  res.send("Movimiento dado de alta")
})


app.get('/Clientes/:idcliente/Datos',function(req,res){

  clienteMLab.get('',function (err,resM,body){
    if(!err){
      res.send(body);
    }else {
      console.log(body);
    }
  })
})


app.post('/Clientes',function(req,res){
//al request se le manda el body
  clienteMLab.post('',req.body,function (err,resM,body){
    res.send(body);

  })
})
app.post('/DatosUsr2',function(req,res){
  console.log('Token desde API /DatosUsr2 = '+tokensito)
  jwt.verify(tokensito,'my_secret_key',function(err,data){
    if(err){
      res.sendStatus(403);
    } else {
    console.log(data.body[0].idcliente);

      //res.json(data)
      var idcli = JSON.stringify(data.body[0].idcliente);
      console.log('en api'+idcli);
    var query = 'q={"idcliente" : '+idcli+'}'
    clienteMLabRaiz=requestjson.createClient(urlMlabRaiz+"Clientes"+apikey+'&'+query)
    console.log(urlMlabRaiz+"Clientes"+apikey+'&'+query)
      clienteMLabRaiz.get('',req.body,function (err,resM,body){
        if(!err){
          res.send(body);
        }else {
          console.log(body);
        }
      })
    }
  })

})
app.get('/DatosUsr/:idcliente',function(req,res){
  //res.send("Aqui tiene al cliente numero : "+req.params.idcliente);

  var idcli = req.params.idcliente//req.body.idcliente
  console.log('en api'+idcli);
var query = 'q={"idcliente" : '+idcli+'}'
clienteMLabRaiz=requestjson.createClient(urlMlabRaiz+"Clientes"+apikey+'&'+query)
console.log(urlMlabRaiz+"Clientes"+apikey+'&'+query)
  clienteMLabRaiz.get('',req.body,function (err,resM,body){
    if(!err){
      res.send(body);
    }else {
      console.log(body);
    }
  })
})

app.get('/protected',function(req,res){
  //tokensito = sessionStorage.getItem('Token')
  console.log('Token desde API /protected = '+tokensito)
  jwt.verify(tokensito,'my_secret_key',function(err,data){
    if(err){
      res.sendStatus(403);
    } else {
    console.log(data.body[0].idcliente);
      res.json(data)
    }
  })

})
//middleware
function ensureToken (req,res,next){
  const bearerHeader = req.headers['authorization'];
  console.log(bearerHeader);
  if(typeof bearerHeader !== 'undefined'){
    const bearer = bearerHeader.split(" ");//creamos arreglo
    const bearerToken = bearer[1];
    req.token = bearerToken;
    next();
  }else{
    res.sendStatus(403)
  }
}


app.post('/Signin',function(req,res){
  //res.set("Access-Control-Allow-Headers","Content-Type");
  var email = req.body.email
  var password = req.body.password
  console.log(req.body)
  //preguntar si estan los datos
  var query = 'q={"email":"'+email+'","password":"'+password+'"}&f={"idcliente":1}'
  console.log(query)

  clienteMLabRaiz=requestjson.createClient(urlMlabRaiz+"Clientes"+apikey+'&'+query)
  console.log(urlMlabRaiz+"Clientes"+apikey+"&"+query)
  clienteMLabRaiz.get('',function (err,resM,body){
    if(!err){
      if (body.length ==1){
        //var user ={body}
        console.log("id cliente"+JSON.stringify(body[0].idcliente))
        //var token = jwt.sign({user},'my_secret_key')
        var token = jwt.sign({body},'my_secret_key')
        //res.status(200).send('Usuario logado')
        sessionStorage.setItem('Token',token)
        tokensito = sessionStorage.getItem('Token')
        console.log('Hola mi token desde API es '+tokensito)
        res.json({token});
        console.log('Token desde API'+token)
      }else {
        res.status(404).send('Usuario no encontrado')
      }

    }
  })
})


app.post('/Login',function(req,res){
  //res.set("Access-Control-Allow-Headers","Content-Type");
  var email = req.body.email
  var password = req.body.password
  //preguntar si estan los datos
  var query = 'q={"email":"'+email+'","password":"'+password+'"}'
  console.log(query)

  clienteMLabRaiz=requestjson.createClient(urlMlabRaiz+"Usuarios"+apikey+'&'+query)
  console.log(urlMlabRaiz+"Usuarios"+apikey+"&"+query)
  clienteMLabRaiz.get('',function (err,resM,body){
    if(!err){
      if (body.length ==1){
        res.status(200).send('Usuario logado')
      }else {
        res.status(404).send('Usuario no encontrado')
      }

    }
  })


})
